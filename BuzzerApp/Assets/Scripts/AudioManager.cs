using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;

    public GameObject AudioPlayer;

    public static AudioManager GetInstance()
    {
        return _instance;
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void ChangeSound(AudioClip audioClip)
    {
        AudioPlayer.GetComponent<AudioSource>().clip = audioClip;
    }

    public void Play()
    {
        AudioPlayer.GetComponent<AudioSource>().Play();
    }

    public void Stop()
    {
        AudioPlayer.GetComponent<AudioSource>().Stop();
    }
}
