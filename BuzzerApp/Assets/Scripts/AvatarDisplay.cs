using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AvatarDisplay : MonoBehaviour
{
    private MainUIManager _mainUIManager;
    private AudioManager _audioUIManager;

    public AvatarScriptableObject avatarScriptableObject;
    public Image image;

    void Start()
    {
        _mainUIManager = MainUIManager.GetInstance();
        _audioUIManager = AudioManager.GetInstance();
        this.image.sprite = avatarScriptableObject.sprite;
    }

    public void SetAvatarId()
    {
        _mainUIManager.Avatar = avatarScriptableObject;
        _mainUIManager.MarkAvatarAsSelected();

        _audioUIManager.ChangeSound(avatarScriptableObject.avatarAudio);
        _audioUIManager.Play();
    }

}
