using UnityEngine;

[CreateAssetMenu(fileName = "Avatar", menuName = "ScriptableObjects/AvatarScriptableObject", order = 1)]
public class AvatarScriptableObject : ScriptableObject
{
    public int avatarId;
    public Sprite sprite;
    public AudioClip avatarAudio;

    //Ability
    public string abilityName;
    public string abilityDescription;
    public Sprite abilityImage;
}