using UnityEngine;

public class BuzzerScript : MonoBehaviour
{
    public float rotationSpeed = 30;

    public void Update()
    {
        transform.localRotation *= Quaternion.AngleAxis(rotationSpeed * Time.deltaTime, Vector3.up);
    }
}
