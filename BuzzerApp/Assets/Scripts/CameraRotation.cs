using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    public GameObject buzzer;
    public float rotationSpeed = 1;

    private void Update()
    {
        if(buzzer != null)
        {
            transform.LookAt(new Vector3(buzzer.transform.position.x, buzzer.transform.position.y - 3, buzzer.transform.position.z));
            transform.Translate(Vector3.right * Time.deltaTime * rotationSpeed);
        }
    }

    public void SetUpTarget(GameObject go)
    {
        buzzer = go;
    }
   
}
