using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class JoinButton : MonoBehaviour
{
    private TMP_Text buttonText;
    private DiscoveryResponse matchInfo;

    private void Awake()
    {
        buttonText = GetComponentInChildren<TMP_Text>();
        GetComponent<Button>().onClick.AddListener(JoinMatch);
    }

    public void Initialize(DiscoveryResponse matchInfo)
    {
        this.matchInfo = matchInfo;
        buttonText.text = matchInfo.name;
    }

    private void JoinMatch()
    {
        FindObjectOfType<NewNetworkDiscoveryHUD>().ConnectToUri(matchInfo.uri);
    }
}
