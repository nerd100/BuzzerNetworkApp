using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class LongClickButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private bool pointerDown;
    private float pointerDownTimer;

    [SerializeField]
    private float requiredHoldTimer;

    public UnityEvent onLongClick;

    private bool vibrate = true;

    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDown = true;
        Debug.Log("Pointer Down");
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Reset();
        Debug.Log("Pointer Up");
    }

    // Update is called once per frame
    void Update()
    {
        if (pointerDown)
        {
            if (vibrate)
            {
                Handheld.Vibrate();
                StartCoroutine(Virbrate());
            }
            pointerDownTimer += Time.deltaTime;
            if(pointerDownTimer >= requiredHoldTimer)
            {
                if(onLongClick != null)
                {
                    onLongClick.Invoke();
                }
                Reset();
            }
        }
    }
    private void Reset()
    {
        pointerDown = false;
        pointerDownTimer = 0;
    }

    IEnumerator Virbrate()
    {
        vibrate = false;
        yield return new WaitForSeconds(0.5f);
        vibrate = true;
    }
}
