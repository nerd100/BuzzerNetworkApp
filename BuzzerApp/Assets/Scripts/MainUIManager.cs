using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class MainUIManager : MonoBehaviour
{
    private static MainUIManager _instance;

    [SerializeField]
    private GameObject TitleView;
    [SerializeField]
    private GameObject SelectionView;
    [SerializeField]
    private GameObject HostView;
    [SerializeField]
    private GameObject FindServerView;
    [SerializeField]
    private GameObject AvatarView;

    [SerializeField]
    private Button hostButton;
    [SerializeField]
    private Button avatarButton;
    [SerializeField]
    private Button findServerButton;

    [SerializeField]
    private Button startServer;
    [SerializeField]
    private Button refreshList;

    [SerializeField]
    private TMP_InputField serverNameInput;
    [SerializeField]
    private TMP_InputField clientNameInput;

    [SerializeField]
    private GameObject AvatarGroup;

    public string ServerName { get; set; }
    public string ClientName { get; set; }

    public AvatarScriptableObject Avatar { get; set; }

    public GameObject AvatarContainer;

    MyNetworkManager myNetworkManager;

    public event Action onChangeAvatar;

    public void ChangeAvatar()
    {
        onChangeAvatar?.Invoke();
    }

    public static MainUIManager GetInstance()
    { 
        return _instance; 
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    private void Start()
    {
        myNetworkManager = FindObjectOfType<MyNetworkManager>();
        TitleView.SetActive(true);
        startServer.onClick.AddListener(myNetworkManager.GetComponent<NewNetworkDiscoveryHUD>().StartHost);
        refreshList.onClick.AddListener(myNetworkManager.GetComponent<NewNetworkDiscoveryHUD>().FindServer);

        onChangeAvatar += MarkAvatarAsSelected;
    }

    private void Update()
    {
        if (serverNameInput.text != string.Empty)
        {
            hostButton.interactable = true;
        }
        else
        {
            hostButton.interactable = false;
        }

        if (clientNameInput.text != string.Empty)
        {
            avatarButton.interactable = true;
        }
        else
        {
            avatarButton.interactable = false;
        }

        if (Avatar != null && Avatar.avatarId != -1)
        {
            findServerButton.interactable = true;
        }
        else
        {
            findServerButton.interactable = false;
        }
    }

    public void ChangeView(int currentView)
    {
        switch ((ViewsEnum)currentView)
        {
            case ViewsEnum.title:
                TitleView.SetActive(true);
                SelectionView.SetActive(false);
                HostView.SetActive(false);
                FindServerView.SetActive(false);
                AvatarView.SetActive(false);
                break;
            case ViewsEnum.selection:
                TitleView.SetActive(false);
                SelectionView.SetActive(true);
                HostView.SetActive(false);
                FindServerView.SetActive(false);
                AvatarView.SetActive(false);
                break;
            case ViewsEnum.host:
                TitleView.SetActive(false);
                SelectionView.SetActive(false);
                HostView.SetActive(true);
                FindServerView.SetActive(false);
                AvatarView.SetActive(false);
                break;
            case ViewsEnum.findserver:
                TitleView.SetActive(false);
                SelectionView.SetActive(false);
                HostView.SetActive(false);
                FindServerView.SetActive(true);
                AvatarView.SetActive(false);
                break;
            case ViewsEnum.avatar:
                TitleView.SetActive(false);
                SelectionView.SetActive(false);
                HostView.SetActive(false);
                FindServerView.SetActive(false);
                AvatarView.SetActive(true);
                break;
        }
    }

    public void SetServerName()
    {
        ServerName = serverNameInput.text;
    }

    public void SetClientName()
    {
        ClientName = clientNameInput.text;
    }

    public void GoToSecretQuizScene()
    {
        Destroy(myNetworkManager);
        LoadSceneManager.Instance.LoadSceneByString("Escape");
    }

    public void MarkAvatarAsSelected()
    {
        foreach (Transform avatarElement in AvatarGroup.transform)
        {
            var avatarElementId = avatarElement.GetComponent<AvatarDisplay>().avatarScriptableObject.avatarId;
            var rahmen = avatarElement.transform.Find("Rahmen");
            if (Avatar.avatarId == avatarElementId)
            {
                rahmen.gameObject.GetComponent<Image>().color = Color.green;
            }
            else
            {
                rahmen.gameObject.GetComponent<Image>().color = Color.white;
            }          
        }

        if (Avatar.avatarId == 0)
        {
            var childCount = AvatarContainer.transform.childCount;
            var randomChild = UnityEngine.Random.Range(2, childCount);
            var child = AvatarContainer.transform.GetChild(randomChild);
            var childAvatar = child.GetComponent<AvatarDisplay>().avatarScriptableObject;
            Debug.Log(childAvatar.avatarId);
            this.Avatar = childAvatar;
        }
        else
        {

        }
    }
}
