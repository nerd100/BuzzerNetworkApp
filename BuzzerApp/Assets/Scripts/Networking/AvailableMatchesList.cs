using System;
using System.Collections.Generic;
using UnityEngine;

public static class AvailableMatchesList
{
    // Start is called before the first frame update
    public static event Action<List<DiscoveryResponse>> OnAvailableMatchesChanged;

    private static List<DiscoveryResponse> matches = new List<DiscoveryResponse>();

    // Update is called once per frame
    public static void HandleNewMatchList(List<DiscoveryResponse> matchList)
    {
        matches = matchList;
        OnAvailableMatchesChanged?.Invoke(matches);
    }
}
