using UnityEngine;
using Mirror;

public class ClientManager : MonoBehaviour
{
    private MyNetworkManager myNetworkManager;
    private Player player;
    private float timer = 0.0f;
    private bool roundRunning = false;

    private string playerName;

    private static ClientManager _instance;

    public static ClientManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        myNetworkManager = FindObjectOfType<MyNetworkManager>();
    }

    public void OnSendTime()
    {
        player = NetworkClient.connection.identity.GetComponent<Player>();
        playerName = myNetworkManager.GetComponent<ClientObject>().ClientName;
        player.CmdSend(playerName + "|" + timer);
        ClientUIManager.Instance.DisableClickPanel();
        roundRunning = false;
    }

    public void OnSendMessage()
    {
        player = NetworkClient.connection.identity.GetComponent<Player>();
        playerName = myNetworkManager.GetComponent<ClientObject>().ClientName;
        string message = ClientUIManager.Instance.answerField.text ?? "";
        player.CmdSend(playerName + "|" + message);
    }

    public void DisconnectClient()
    {
        if (NetworkClient.isConnected)
        {
            var networkDiscovery = myNetworkManager.GetComponent<NewNetworkDiscoveryHUD>().networkDiscovery;
            myNetworkManager.StopClient();
            networkDiscovery.StopDiscovery();
            Destroy(myNetworkManager);

            LoadSceneManager.Instance.LoadSceneByString("Main");
        }
    }

    public void StartRound()
    {
        if (ClientUIManager.Instance.isBuzzerRound)
        {
            ClientUIManager.Instance.EnableClickPanel();
            timer = 0.0f;
            roundRunning = true;
        }
    }

    private void Update()
    {
        if (roundRunning)
        {
            timer += Time.deltaTime;
        }
    }
}
