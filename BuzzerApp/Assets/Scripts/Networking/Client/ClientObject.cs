using UnityEngine;

public class ClientObject : MonoBehaviour
{
    public string ClientName { get; set; }
    public Color BuzzerColor { get; set; }
    public int AvatarId { get; set; }

    public AvatarScriptableObject Avatar{ get; set; }
}
