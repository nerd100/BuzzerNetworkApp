using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ClientUIManager : MonoBehaviour
{
    public static ClientUIManager Instance { get; private set; }

    private MyNetworkManager myNetworkManager;

    public GameObject propertiesPanel;
    public GameObject clickPanel;
    public TMP_Text playerName;
    public GameObject buzzerLight;
    public bool isBuzzerRound = true;

    public GameObject buzzer;
    public GameObject textPanel;

    public TMP_InputField answerField;
    public Image avatarImage;

    //Ability
    public TMP_Text abilityName;
    public TMP_Text abilityDescription;
    public Image abilitySymbol;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Debug.Log("Warning: multiple " + this + " in scene!");
        }

        myNetworkManager = FindObjectOfType<MyNetworkManager>();
    }

    private void Start()
    {
        this.propertiesPanel.SetActive(false);
        var clientObject = myNetworkManager.GetComponent<ClientObject>();
        playerName.text = clientObject.ClientName;
        Debug.Log(clientObject.Avatar);
        avatarImage.sprite = clientObject.Avatar.sprite;
        abilityName.text = clientObject.Avatar.abilityName;
        abilityDescription.text = clientObject.Avatar.abilityDescription;
        abilitySymbol.sprite = clientObject.Avatar.abilityImage;
    }

    public void OpenProperties()
    {
        this.propertiesPanel.SetActive(true);
    }

    public void CloseProperties()
    {
        this.propertiesPanel.SetActive(false);
    }

    public void DisableClickPanel()
    {
        clickPanel.SetActive(false);
		buzzerLight.SetActive(false);
        Transform podest = buzzer.transform.GetChild(0);
        podest.GetComponent<MeshRenderer>().material.color = Color.black;
        podest.GetComponent<MeshRenderer>().material.EnableKeyword("_Emission");
        podest.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
    }

    public void EnableClickPanel()
    {
        clickPanel.SetActive(true);
		buzzerLight.SetActive(true);
        Transform podest = buzzer.transform.GetChild(0);
        podest.GetComponent<MeshRenderer>().material.color = Color.white;
        podest.GetComponent<MeshRenderer>().material.EnableKeyword("_Emission");
        podest.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.white);
    }

    public void BuzzerRound()
    {
        isBuzzerRound = true;
        buzzer.SetActive(true);
        textPanel.SetActive(false);
    }

    public void TextRound()
    {
        isBuzzerRound = false;
        buzzer.SetActive(false);
        textPanel.SetActive(true);
    }
}
