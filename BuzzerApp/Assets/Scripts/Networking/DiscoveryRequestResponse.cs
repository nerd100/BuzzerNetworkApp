using System;
using Mirror;
using System.Net;

public class DiscoveryRequest : NetworkMessage
{
    public string language = "en";

    // Add properties for whatever information you want sent by clients
    // in their broadcast messages that servers will consume.
}

public class DiscoveryResponse : NetworkMessage
{
    public IPEndPoint EndPoint { get; set; }

    public Uri uri;

    public string name;

    public long serverId;
}