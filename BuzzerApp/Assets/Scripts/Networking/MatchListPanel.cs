using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchListPanel : MonoBehaviour
{
    [SerializeField] private GameObject joinButtonPrefab;


    private void Awake()
    {
        AvailableMatchesList.OnAvailableMatchesChanged += AvailableMatchesList_OnAvailableMatchesChanged;
    }

    void OnDestroy()
    {
        AvailableMatchesList.OnAvailableMatchesChanged -= AvailableMatchesList_OnAvailableMatchesChanged;
    }

    private void AvailableMatchesList_OnAvailableMatchesChanged(List<DiscoveryResponse> matches)
    {
        this.ClearExistingButtons();
        CreateNewJoinGameButtons(matches);
    }

    private void ClearExistingButtons()
    {
        var buttons = GetComponentsInChildren<JoinButton>();
        foreach (var button in buttons)
        {
            Destroy(button.gameObject);
        }
    }

    private void CreateNewJoinGameButtons(List<DiscoveryResponse> matches)
    {
        foreach (var match in matches)
        {
            var button = Instantiate(joinButtonPrefab);
            button.transform.SetParent(this.transform);
            button.transform.localScale = new Vector3(1, 1, 1);
            button.GetComponent<JoinButton>().Initialize(match);
        }
    }
}
