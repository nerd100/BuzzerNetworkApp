using System;
using UnityEngine;
using Mirror;

public class MyNetworkManager : NetworkManager
{
    public static event Action<string> OnMessage;

    public struct CreatePlayerMessage : NetworkMessage
    {
        public string name;
    }

    public override void OnStartServer()
    {
        base.OnStartServer();
        NetworkServer.RegisterHandler<CreatePlayerMessage>(OnCreatePlayer);
    }

    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);

        // tell the server to create a player with this name
        var clientName = GetComponent<ClientObject>().ClientName;
        conn.Send(new CreatePlayerMessage { name = clientName });
    }

    void OnCreatePlayer(NetworkConnection connection, CreatePlayerMessage createPlayerMessage)
    {
        Debug.Log(createPlayerMessage.name);
        OnMessage?.Invoke(createPlayerMessage.name ?? "defaultPlayer");
    }
}
