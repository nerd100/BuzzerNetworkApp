using System;
using System.Linq;
using Mirror;
using System.Collections.Generic;
using UnityEngine;

public class NewNetworkDiscoveryHUD : MonoBehaviour
{
    public NewNetworkDiscovery networkDiscovery;
    readonly Dictionary<long, DiscoveryResponse> discoveredServers = new Dictionary<long, DiscoveryResponse>();

    public ServerObject ServerObject { get; set; }
    public ClientObject ClientObject { get; set; }

    private MainUIManager mainUIManager;

    private bool triggerRefresh = false;

    private void Awake()
    {
        mainUIManager = MainUIManager.GetInstance();
    }

#if UNITY_EDITOR
    void OnValidate()
    {
        if (networkDiscovery == null)
        {
            networkDiscovery = GetComponent<NewNetworkDiscovery>();
            UnityEditor.Events.UnityEventTools.AddPersistentListener(networkDiscovery.OnServerFound, OnDiscoveredServer);
            UnityEditor.Undo.RecordObjects(new UnityEngine.Object[] { this, networkDiscovery }, "Set NetworkDiscovery");
        }
    }
#endif

    private void Update()
    {
        //if servers discoverd and refreshButton clicked, instantiate buttons to connect in scrollview
        if (!NetworkClient.isConnected && !NetworkServer.active && !NetworkClient.active && triggerRefresh && discoveredServers.Count != 0)
        {
            var matchList = discoveredServers.Values;
            AvailableMatchesList.HandleNewMatchList(matchList.ToList());

            triggerRefresh = false;
        }
    }

    public void StartHost()
    {
        ServerObject = gameObject.AddComponent<ServerObject>();
        ServerObject.ServerName = mainUIManager.ServerName;

        discoveredServers.Clear();
        NetworkManager.singleton.StartServer();
        networkDiscovery.AdvertiseServer();
        LoadSceneManager.Instance.LoadSceneByString("HostScene");
    }

    public void FindServer()
    {
        discoveredServers.Clear();
        networkDiscovery.StartDiscovery();
        triggerRefresh = true;
    }

    public void AddClientObject()
    {
        ClientObject = gameObject.AddComponent<ClientObject>();
        ClientObject.ClientName = mainUIManager.ClientName;
        ClientObject.BuzzerColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        ClientObject.Avatar = mainUIManager.Avatar;
    }

    public void ConnectToUri(Uri uri)
    {
        networkDiscovery.StopDiscovery();
        NetworkManager.singleton.StartClient(uri);
        LoadSceneManager.Instance.LoadSceneByString("ClientScene");
    }

    public void OnDiscoveredServer(DiscoveryResponse info)
    {
        // Note that you can check the versioning to decide if you can connect to the server or not using this method
        discoveredServers[info.serverId] = info;
    }
}
