using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ServerUIManager : MonoBehaviour
{
    public GameObject propertiesPanel;
    public GameObject scrollViewContent;
    public GameObject answerPrefab;

    private static ServerUIManager _instance;

    public static ServerUIManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void OpenProperties()
    {
        this.propertiesPanel.SetActive(true);
    }

    public void CloseProperties()
    {
        this.propertiesPanel.SetActive(false);
    }

    public void AddAnswer(string answer)
    {
        Debug.Log(answer);
        var answerOfPlayer = answer.Split('|');
        var answerGO = Instantiate(answerPrefab);
        answerGO.transform.SetParent(scrollViewContent.transform);
        answerGO.transform.localScale = new Vector3(1, 1, 1);
        answerGO.transform.GetChild(1).GetComponent<TMP_Text>().text = answerOfPlayer[0];
        answerGO.transform.GetChild(2).GetComponent<TMP_Text>().text = answerOfPlayer[1];
    }

    public void ClearAnswerPanel()
    {
        foreach (Transform child in scrollViewContent.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}
