using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;

public class Player : NetworkBehaviour
{
    public GameObject buzzer;
    private MyNetworkManager myNetworkManager;

    private void Awake()
    {
        myNetworkManager = FindObjectOfType<MyNetworkManager>();
    }
    private void Start()
    {
        if(isLocalPlayer == true)
        {
            CreateBuzzer();
        }
    }

    [Command]
    public void CmdSend(string message)
    {
        if (message.Trim() != "" && isServerOnly)
        { 
            ServerUIManager.Instance.AddAnswer(message.Trim());
        }
        Handheld.Vibrate();
    }

    private void CreateBuzzer()
    {
        GameObject buzzerObject = Instantiate(buzzer);
        Color buzzerColor = myNetworkManager.GetComponent<ClientObject>().BuzzerColor;
        Transform buzzerTop = buzzerObject.transform.GetChild(1);
        buzzerTop.GetComponent<MeshRenderer>().material.color = buzzerColor;
        buzzerTop.GetComponent<MeshRenderer>().material.EnableKeyword("_Emission");
        buzzerTop.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", buzzerColor);
        Debug.Log(buzzerColor);
        ClientUIManager.Instance.buzzer = buzzerObject;
    }

    public void ActivateButtons()
    {
        if (!isServer) return;

        RpcActivateBuzzer();
    }

    [ClientRpc]
    public void RpcActivateBuzzer()
    {
        ClientManager.Instance.StartRound();
    }
}
