using System;
using System.Collections.Generic;

public static class QuizCodeRessources
{
    public static IDictionary<string, string> CodeDic = new Dictionary<string, string>(){
        //L�sungen
        {"7592", "�ffne den Umschlag\nFunktioniert nur in eine Richtung\nU -> 4\nS -> 3\nB -> 9\nC -> 2\nK -> 6\nF -> 0\nA -> 5\nH -> 8\nD -> 1\nL -> 7"},
        {"1630", "�ffne Brief 2\n suche hier weiter -> https://imgur.com/gallery/"},
        {"5491", "blau = 4"},
        {"1121", "gelb = 3"},
        {"3740", "�ffne Brief 3"},
        {"4098", "Befreie dich!"},
        {"6687", "Befreie dich!"},
        //Timer stop
        {"4176", "Worauf wartest du, �ffne die Box!"},
        //Hinweise
        {"1890", "Ich bin ein Pr�sident."},
        {"1795", "In diesem Jahr wurde ich geboren."},
        {"1884", "Unsere Namen haben eine Gemeinsamkeit."},
        {"1917", "Nutze deine vorhandenen Hilfsmittel."},
        //Quatsch
        {"1234", "Ich denke du nimmst das nicht ganz ernst."},
        {"1337", "Guter Versuch! Aber zu einfach."},
        {"2021", "Immerhin wei�t du welches Jahr wir haben."},
        {"666", "Ich bin der Teufel."},
        {"1992", "Viel zu offensichtlich"},
        {"1111", "Du magst wohl die 1."},
        {"2222", "Du magst wohl die 2."},
        {"3333", "Du magst wohl die 3."},
        {"4444", "Du magst wohl die 4."},
        {"5555", "Du magst wohl die 5."},
        {"6666", "Du magst wohl die 6."},
        {"7777", "Du magst wohl die 7."},
        {"8888", "Du magst wohl die 8."},
        {"9999", "Du magst wohl die 9."},
        {"0000", "Komm in meine WhatsApp Gruppe und ich zeige dir wie du ein passives Einkommen von 3000� generierst."},
        //EasterEggs
        {"007", "Mein Name ist Bond, Jamed Bond.\n[EasterEgg +5 Punkte]"},
        {"3141", "Life of Pi.\n[EasterEgg +5 Punkte]"},
        //Bonus
        {"2713", "Sehr gut aufgepasst\n[+5 Punkte]"},
        {"7340", "Leetspeak\n[+5 Punkte]"},
    };
}
