using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using TMPro;
using System.Collections.Generic;

public class QuizManager : MonoBehaviour
{
	public static QuizManager Instance { get; private set; }

	public TMP_Text timerText;
	public TMP_InputField inputText;
	public TMP_Text outputText;
	float timer = 180.0f;
	string minutesString = "";
	string secondsString = "";
	
	DateTime minimizedDate;
	bool minimizedDateSet = false;
	private float pastSeconds;

    private bool isCountdown = true;
	private bool pauseGame = false;
	private bool stopCountdown = false;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
		else
		{
			Debug.Log("Warning: multiple " + this + " in scene!");
		}
	}

	// Start is called before the first frame update
	void Start()
    {
		Application.runInBackground = true;
		outputText.text = "";

	}
	
	void OnApplicationFocus (bool isGameFocus)
    {
		if(isGameFocus && minimizedDateSet && isCountdown)
		{
			pastSeconds = (float)(DateTime.Now - minimizedDate).TotalSeconds;
			timer -= pastSeconds;
			Debug.Log(pastSeconds);
		}
		else if(isGameFocus && minimizedDateSet && !isCountdown)
        {
			pastSeconds = (float)(DateTime.Now - minimizedDate).TotalSeconds;
			timer += pastSeconds;
			Debug.Log(pastSeconds);
		}
		else
		{
			minimizedDate = DateTime.Now;
			minimizedDateSet = true;
		}
    }

    // Update is called once per frame
    void Update()
    {	
        if (!pauseGame && !stopCountdown)
        {
			CalculateCountdown();
		}

		if(timer <= 0 && isCountdown)
        {
			StartEscapeRoomAfterTimer();
        }
    }

	public void CalculateCountdown()
    {
		float minutes = Mathf.Floor(timer / 60);
		float seconds = Mathf.RoundToInt(timer % 60);

		if (minutes < 10)
		{
			minutesString = "0" + minutes.ToString();
		}
		else
		{
			minutesString = minutes.ToString();
		}
		if (seconds < 10)
		{
			secondsString = "0" + Mathf.RoundToInt(seconds).ToString();
		}
		else
		{
			secondsString = Mathf.RoundToInt(seconds).ToString();
		}

		timerText.text = minutesString + ":" + secondsString;

		if (isCountdown)
		{
			timer -= Time.deltaTime;
		}
		else
		{
			timer += Time.deltaTime;
		}
	}

	public void ApplyCode(){

		string code = inputText.text;
		string output = "";
		QuizCodeRessources.CodeDic.TryGetValue(code, out output);

        if (output == "" || output == null){
			outputText.text = "Error";
		}
		else
		{
			outputText.text = output;
		}

		if(code == "7592" && isCountdown)
        {
			StartEscapeRoom();
        }

		if(code == "4176")
        {
			stopCountdown = true;
        }
	}

	public void StartEscapeRoom()
    {
		timer = 0;
		isCountdown = false;
    }

	public void StartEscapeRoomAfterTimer()
	{
		timer = 0;
		timerText.text = "Err";
		outputText.text = "Error";
		isCountdown = false;
		StartCoroutine(PauseCoroutine());
	}

	IEnumerator PauseCoroutine()
	{
		pauseGame = true;
		yield return new WaitForSeconds(3);
		pauseGame = false;
		inputText.text = "7592";
		ApplyCode();
	}
}
