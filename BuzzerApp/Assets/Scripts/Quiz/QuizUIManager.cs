using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizUIManager : MonoBehaviour
{
    public GameObject propertiesPanel;

    public void OpenPropertiesPanel()
    {
        propertiesPanel.SetActive(true);
    }

    public void ClosePropertiesPanel()
    {
        propertiesPanel.SetActive(false);
    }

    public void Copy()
    {
        GUIUtility.systemCopyBuffer = QuizManager.Instance.outputText.text;
    }

    public void ChangeScene()
    {
        LoadSceneManager.Instance.LoadSceneByString("Main");
    }
}
