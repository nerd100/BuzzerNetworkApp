using UnityEngine;
using TMPro;
using Mirror;

public class ServerManager : MonoBehaviour
{
    public GameObject canvas;
    public GameObject playerJoinFadeOut;
    private MyNetworkManager myNetworkManager;

    public void Awake()
    {
        MyNetworkManager.OnMessage += OnJoinPlayer;
        myNetworkManager = FindObjectOfType<MyNetworkManager>();
    }

    void OnJoinPlayer(string message)
    {
        //Create Text of Joined Player
        var textObject = Instantiate(playerJoinFadeOut, new Vector3(0,0,0), Quaternion.identity, canvas.transform);
        textObject.transform.localPosition = new Vector3(0, 0, 0);
        textObject.GetComponentInChildren<TMP_Text>().text = message + " " + "joined!";
    }

    public void StartTimerRound()
    {
        var player = FindObjectOfType<Player>();
        if (player == null)
            return;
        player.RpcActivateBuzzer();
    }

    public void ClearAnswerPanel()
    {
        ServerUIManager.Instance.ClearAnswerPanel();
    }

    public void CloseRoom()
    {
        var networkDiscovery = myNetworkManager.GetComponent<NewNetworkDiscoveryHUD>().networkDiscovery;

        if (NetworkServer.active && NetworkClient.isConnected)
        {
            NetworkManager.singleton.StopHost();
            networkDiscovery.StopDiscovery();
        }
        else if (NetworkServer.active)
        {
            NetworkManager.singleton.StopServer();
            networkDiscovery.StopDiscovery();
        }

        Destroy(myNetworkManager);
        LoadSceneManager.Instance.LoadSceneByString("Main");
    }
}
