using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextFadeOut : MonoBehaviour
{
    private Vector3 position;
    private TMP_Text text;
    private Color textColor;

    [SerializeField]
    private float upSpeed = 30f;

    void Start()
    {
        position = this.gameObject.transform.position;
        text = this.GetComponentInChildren<TMP_Text>();
        textColor = text.color;
        Destroy(gameObject, 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        position += Vector3.up * upSpeed * Time.deltaTime;
        this.gameObject.transform.position = position;

        var newAlphaValue = textColor.a - textColor.a * 0.005f;
        text.color = new Color(textColor.r, textColor.g, textColor.b, newAlphaValue);
        textColor = text.color;
    }
}
