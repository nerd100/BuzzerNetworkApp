using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIScript : MonoBehaviour
{
    public GameObject TitleView;
    public GameObject SelectionView;
    public GameObject HostView;
    public GameObject FindServerView;

    public Button hostButton;
    public TMP_InputField hostInputField;

    public TMP_InputField clientNameInputField;
    public Button clientButton;

    public Button startServer;
    public Button refreshList;

    // Start is called before the first frame update
    void Start()
    {
        TitleView.SetActive(true);
        MyNetworkManager myNetworkManager = FindObjectOfType<MyNetworkManager>();
        startServer.onClick.AddListener(myNetworkManager.GetComponent<NewNetworkDiscoveryHUD>().StartHost);
        refreshList.onClick.AddListener(myNetworkManager.GetComponent<NewNetworkDiscoveryHUD>().FindServer);
    }

    private void Update()
    {
        if(hostInputField.text != string.Empty)
        {
            hostButton.interactable = true;
        }
        else
        {
            hostButton.interactable = false;
        }

        if (clientNameInputField.text != string.Empty)
        {
            clientButton.interactable = true;
        }
        else
        {
            clientButton.interactable = false;
        }
    }

    public void ChangeView(int currentView)
    {
        switch ((ViewsEnum)currentView)
        {
            case ViewsEnum.title:
                TitleView.SetActive(true);
                SelectionView.SetActive(false);
                HostView.SetActive(false);
                FindServerView.SetActive(false);
                break;
            case ViewsEnum.selection:
                TitleView.SetActive(false);
                SelectionView.SetActive(true);
                HostView.SetActive(false);
                FindServerView.SetActive(false);
                break;
            case ViewsEnum.host:
                TitleView.SetActive(false);
                SelectionView.SetActive(false);
                HostView.SetActive(true);
                FindServerView.SetActive(false);
                break;
            case ViewsEnum.findserver:
                TitleView.SetActive(false);
                SelectionView.SetActive(false);
                HostView.SetActive(false);
                FindServerView.SetActive(true);
                break;

        }
    }
}
